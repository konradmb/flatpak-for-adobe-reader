# This one no longer works, but good news: there's a new wrapper published on Flathub https://flathub.org/apps/com.adobe.Reader
## (which I've contributed to also)

# <img src="build/files/Adobe_Reader_v9.0_icon.png" alt="Adobe Reader 9 Icon" width="100" /> Flatpak for Adobe Reader 9

This is the last version of Reader that Adobe provided before abandoning Linux releases. It's wrapped in Flatpak, so it's easier to install and use, without any library errors.

This package is not verified, endorsed nor made by Adobe.

## How to install

### STOP! Read the following paragraph. It's not the usual gibberish.

**There may be some security bugs, which are known but not fixed. Use it only for documents that you trust.**
<br />
<br />
If you understand all of the consequences of runnning outdated software, proceed. It should be somewhat safe, but remember that bad things may happen.

1. Add repository
    ```
	flatpak remote-add --if-not-exists konradmb https://konradmb.gitlab.io/flatpak-repo/konradmb.flatpakrepo
    ```
2. Install com.adobe.Reader
    ```
	flatpak install konradmb com.adobe.Reader
    ```
## Notes

 - Icon taken from [Wikimedia page](https://commons.wikimedia.org/wiki/File:Adobe_Reader_v9.0_icon.png).  
 - Sample docs on screenshots from [Flatpak documentation](http://readthedocs.org/projects/flatpak/downloads/pdf/latest/) and [iText samples](https://github.com/itext/i7ns-samples/blob/develop/itext/itext.samples/resources/sandbox/acroforms/cmp_purchase_order_filled.pdf).
